<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YjU(12[$M=J:gKfqO{z2d3g.(mGv.i!Y/:|Rb``.$10?^1HpHcLz`^d0)2kzo ]e');
define('SECURE_AUTH_KEY',  '`*-<z&k+,5RwaQ/yW[:3:|>L?J:Auf$x,-u;p+_eG_n|-!6L*x]Qo;8H6{A][tcW');
define('LOGGED_IN_KEY',    '!H_nL]ERQ/GKs1sN05-}{H;~&KSUiP#*/Tq7q IzLqS5Hx<2cf#``p(!;?ZwW@.#');
define('NONCE_KEY',        'Wk&xQc{(NAw)?%aJJh+BaWE_v,Y&<~HO!0&L2-9{a=fV9<=P=m+!Haz%6_8B|0]:');
define('AUTH_SALT',        'se?0+)TI+qy|2^4+!SoZc^,lkJ4ir9MXU=9VanamN2I,qjES~oaNaJ9w-frDAa{G');
define('SECURE_AUTH_SALT', 'gL*-* ?m0UuTD(KW%=>q9VQxr#{~E>myE#IG(u7XL_T]O]L|XkxvAZ Es8ogjXaY');
define('LOGGED_IN_SALT',   'fu#W3LtTUZ[wVVU7l*<AH{2OH6]]cpq,Ia2 DXBa9[I/ByGFC)T1;DTA|f]oK*-k');
define('NONCE_SALT',       '/Epxb_j0(!R]A#fr#8 $f^)rlQ97,u>&*1$k!h()I0f9<4)Vi9tcJZ7Iw~0?MG6*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
